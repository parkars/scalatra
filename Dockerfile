FROM tomcat
MAINTAINER Shaed

RUN apt-get update -y && apt-get upgrade -y && apt-get clean -y

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
